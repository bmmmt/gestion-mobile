package com.example.hopital;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    Button btnConnexion=findViewById(R.id.btnConnexion);
    btnConnexion.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v){
            EditText saisieMdp= findViewById(R.id.mdpSaisie);
            String laSaisie = saisieMdp.getText().toString();
            try {
                if (identifiant.compareTo(unePasserelle.checkUser(identifiant)) == 0) {
                    if (motDePasse.compareTo(unePasserelle.checkPassword(identifiant)) == 0) {
                        //Rediriger l'utilisateur à la page correspondant à son service
                        if (unePasserelle.service(identifiant, motDePasse).compareTo("Admin") == 0) {
                            MenuAdminForm Affichage = new MenuAdminForm();
                            Affichage.setVisible(true);
                            this.setVisible(false);
                        } else if (unePasserelle.service(identifiant, motDePasse).compareTo("Aide soignant") == 0) {
                            MenuForm Affichage = new MenuForm();
                            Affichage.setVisible(true);
                            this.setVisible(false);
                        } else if (unePasserelle.service(identifiant, motDePasse).compareTo("Cuisinier") == 0) {
                            NbRepasRestaurationForm Affichage = new NbRepasRestaurationForm();
                            Affichage.setVisible(true);
                            this.setVisible(false);
                        }
                        else{
                            Toast.makeText(this, "Vous n'êtes pas autorisé à accéder cette application.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(this, "Vous n'avez pas le bon mot de passe.", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(this, "Vous n'avez pas le bon identifiant.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e){
                Toast.makeText(this,"Data not inserted" + e.getMessage(),Toast.LENGTH_LONG).show();
                Toast.makeText(this, "Vous n'avez pas accès à la base de donnée.", Toast.LENGTH_LONG).show();
            }
        }
    });


}
